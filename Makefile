image_registry := docker.io
image_path := codeprac/docker

# use this to override the above
-include ./Makefile.properties

commit_sha = $(shell git rev-parse HEAD | head -c 8)
date = $(shell date +'%Y.%m.%d')
image_tag = $(date)-$(commit_sha)
image_url := $(image_registry)/$(image_path):$(image_tag)

# builds this image
build:
	docker build --network host --tag $(image_path):latest .

# lints this image for best-practices
lint:
	hadolint ./Dockerfile

# tests this iamge for structure integrity
test:
	container-structure-test test --config ./.Dockerfile.yaml --image $(image_path):latest

# scans this image for known vulnerabilities
scan:
	trivy image --clear-cache
	trivy image --format json --output .trivy.json $(image_path):latest
	trivy image --ignore-unfixed --severity HIGH,CRITICAL $(image_path):latest

# publishes this image using the date/time stamp
publish: build
	docker tag $(image_path):latest $(image_url)
	docker push $(image_url)

# publishes this image using the commit sha without building
publish-ci:
	docker tag $(image_path):latest $(image_registry)/$(image_path):$(commit_sha)
	docker push $(image_registry)/$(image_path):$(commit_sha)

# exports this image into a tarball (use in ci cache)
export: build
	docker save $(image_path):latest -o ./image.tar.gz

# import this image from a tarball (use in ci cache)
import:
	mkdir -p ./images
	-docker load -i ./image.tar.gz
